﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Management_System.BL_Classes
{
    internal class Storage
    {
        public int StorageID { get; set; }
        public string StorageLocation { get; set; }
        public int Capacity { get; set; }
    }
}
