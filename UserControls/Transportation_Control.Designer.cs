﻿namespace Inventory_Management_System.UserControls
{
    partial class Transportation_Control
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.Confirm_btn = new Guna.UI2.WinForms.Guna2Button();
            this.label5 = new System.Windows.Forms.Label();
            this.guna2Shapes5 = new Guna.UI2.WinForms.Guna2Shapes();
            this.txtArrival = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.guna2Shapes4 = new Guna.UI2.WinForms.Guna2Shapes();
            this.txtDriver = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.guna2Shapes3 = new Guna.UI2.WinForms.Guna2Shapes();
            this.txtDeparture = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2Shapes1 = new Guna.UI2.WinForms.Guna2Shapes();
            this.txtVehicle = new Guna.UI2.WinForms.Guna2TextBox();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.Confirm_btn);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.guna2Shapes5);
            this.panel2.Controls.Add(this.txtArrival);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.guna2Shapes4);
            this.panel2.Controls.Add(this.txtDriver);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.guna2Shapes3);
            this.panel2.Controls.Add(this.txtDeparture);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.guna2Shapes1);
            this.panel2.Controls.Add(this.txtVehicle);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(834, 540);
            this.panel2.TabIndex = 20;
            // 
            // Confirm_btn
            // 
            this.Confirm_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Confirm_btn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Confirm_btn.BorderRadius = 5;
            this.Confirm_btn.BorderThickness = 1;
            this.Confirm_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Confirm_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Confirm_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Confirm_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Confirm_btn.FillColor = System.Drawing.Color.White;
            this.Confirm_btn.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Confirm_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Confirm_btn.Location = new System.Drawing.Point(332, 444);
            this.Confirm_btn.Name = "Confirm_btn";
            this.Confirm_btn.Size = new System.Drawing.Size(180, 45);
            this.Confirm_btn.TabIndex = 31;
            this.Confirm_btn.Text = "Confirm";
            this.Confirm_btn.Click += new System.EventHandler(this.Confirm_btn_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.label5.Location = new System.Drawing.Point(94, 345);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(193, 28);
            this.label5.TabIndex = 26;
            this.label5.Text = "Arrival Location";
            // 
            // guna2Shapes5
            // 
            this.guna2Shapes5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2Shapes5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.guna2Shapes5.LineThickness = 2;
            this.guna2Shapes5.Location = new System.Drawing.Point(330, 360);
            this.guna2Shapes5.Name = "guna2Shapes5";
            this.guna2Shapes5.PolygonSkip = 1;
            this.guna2Shapes5.Rotate = 0F;
            this.guna2Shapes5.Shape = Guna.UI2.WinForms.Enums.ShapeType.Line;
            this.guna2Shapes5.Size = new System.Drawing.Size(351, 13);
            this.guna2Shapes5.TabIndex = 28;
            this.guna2Shapes5.Text = "guna2Shapes5";
            this.guna2Shapes5.Zoom = 80;
            // 
            // txtArrival
            // 
            this.txtArrival.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtArrival.BorderColor = System.Drawing.Color.White;
            this.txtArrival.BorderThickness = 0;
            this.txtArrival.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtArrival.DefaultText = "";
            this.txtArrival.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txtArrival.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txtArrival.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtArrival.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtArrival.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtArrival.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.txtArrival.ForeColor = System.Drawing.Color.Black;
            this.txtArrival.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtArrival.Location = new System.Drawing.Point(350, 333);
            this.txtArrival.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtArrival.Name = "txtArrival";
            this.txtArrival.PasswordChar = '\0';
            this.txtArrival.PlaceholderForeColor = System.Drawing.Color.DimGray;
            this.txtArrival.PlaceholderText = "Sialkot";
            this.txtArrival.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtArrival.SelectedText = "";
            this.txtArrival.Size = new System.Drawing.Size(307, 29);
            this.txtArrival.TabIndex = 27;
            this.txtArrival.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.label4.Location = new System.Drawing.Point(108, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 28);
            this.label4.TabIndex = 23;
            this.label4.Text = "Driver Name";
            // 
            // guna2Shapes4
            // 
            this.guna2Shapes4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2Shapes4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.guna2Shapes4.LineThickness = 2;
            this.guna2Shapes4.Location = new System.Drawing.Point(334, 198);
            this.guna2Shapes4.Name = "guna2Shapes4";
            this.guna2Shapes4.PolygonSkip = 1;
            this.guna2Shapes4.Rotate = 0F;
            this.guna2Shapes4.Shape = Guna.UI2.WinForms.Enums.ShapeType.Line;
            this.guna2Shapes4.Size = new System.Drawing.Size(351, 13);
            this.guna2Shapes4.TabIndex = 25;
            this.guna2Shapes4.Text = "guna2Shapes4";
            this.guna2Shapes4.Zoom = 80;
            // 
            // txtDriver
            // 
            this.txtDriver.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtDriver.BorderColor = System.Drawing.Color.White;
            this.txtDriver.BorderThickness = 0;
            this.txtDriver.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDriver.DefaultText = "";
            this.txtDriver.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txtDriver.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txtDriver.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtDriver.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtDriver.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtDriver.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.txtDriver.ForeColor = System.Drawing.Color.Black;
            this.txtDriver.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtDriver.Location = new System.Drawing.Point(354, 171);
            this.txtDriver.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDriver.Name = "txtDriver";
            this.txtDriver.PasswordChar = '\0';
            this.txtDriver.PlaceholderForeColor = System.Drawing.Color.DimGray;
            this.txtDriver.PlaceholderText = "Bota";
            this.txtDriver.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDriver.SelectedText = "";
            this.txtDriver.Size = new System.Drawing.Size(307, 29);
            this.txtDriver.TabIndex = 24;
            this.txtDriver.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.label3.Location = new System.Drawing.Point(82, 266);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 28);
            this.label3.TabIndex = 20;
            this.label3.Text = "Departure Location";
            // 
            // guna2Shapes3
            // 
            this.guna2Shapes3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2Shapes3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.guna2Shapes3.LineThickness = 2;
            this.guna2Shapes3.Location = new System.Drawing.Point(334, 281);
            this.guna2Shapes3.Name = "guna2Shapes3";
            this.guna2Shapes3.PolygonSkip = 1;
            this.guna2Shapes3.Rotate = 0F;
            this.guna2Shapes3.Shape = Guna.UI2.WinForms.Enums.ShapeType.Line;
            this.guna2Shapes3.Size = new System.Drawing.Size(351, 13);
            this.guna2Shapes3.TabIndex = 22;
            this.guna2Shapes3.Text = "guna2Shapes3";
            this.guna2Shapes3.Zoom = 80;
            // 
            // txtDeparture
            // 
            this.txtDeparture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtDeparture.BorderColor = System.Drawing.Color.White;
            this.txtDeparture.BorderThickness = 0;
            this.txtDeparture.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDeparture.DefaultText = "";
            this.txtDeparture.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txtDeparture.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txtDeparture.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtDeparture.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtDeparture.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtDeparture.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.txtDeparture.ForeColor = System.Drawing.Color.Black;
            this.txtDeparture.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtDeparture.Location = new System.Drawing.Point(354, 254);
            this.txtDeparture.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDeparture.Name = "txtDeparture";
            this.txtDeparture.PasswordChar = '\0';
            this.txtDeparture.PlaceholderForeColor = System.Drawing.Color.DimGray;
            this.txtDeparture.PlaceholderText = "Lahore";
            this.txtDeparture.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDeparture.SelectedText = "";
            this.txtDeparture.Size = new System.Drawing.Size(307, 29);
            this.txtDeparture.TabIndex = 21;
            this.txtDeparture.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.label1.Location = new System.Drawing.Point(111, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Vehicle Name";
            // 
            // guna2Shapes1
            // 
            this.guna2Shapes1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2Shapes1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.guna2Shapes1.LineThickness = 2;
            this.guna2Shapes1.Location = new System.Drawing.Point(330, 120);
            this.guna2Shapes1.Name = "guna2Shapes1";
            this.guna2Shapes1.PolygonSkip = 1;
            this.guna2Shapes1.Rotate = 0F;
            this.guna2Shapes1.Shape = Guna.UI2.WinForms.Enums.ShapeType.Line;
            this.guna2Shapes1.Size = new System.Drawing.Size(351, 13);
            this.guna2Shapes1.TabIndex = 16;
            this.guna2Shapes1.Text = "guna2Shapes1";
            this.guna2Shapes1.Zoom = 80;
            // 
            // txtVehicle
            // 
            this.txtVehicle.AccessibleName = "txtDriverName";
            this.txtVehicle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtVehicle.BorderColor = System.Drawing.Color.White;
            this.txtVehicle.BorderThickness = 0;
            this.txtVehicle.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtVehicle.DefaultText = "";
            this.txtVehicle.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txtVehicle.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txtVehicle.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtVehicle.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtVehicle.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtVehicle.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.txtVehicle.ForeColor = System.Drawing.Color.Black;
            this.txtVehicle.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtVehicle.Location = new System.Drawing.Point(350, 94);
            this.txtVehicle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.PasswordChar = '\0';
            this.txtVehicle.PlaceholderForeColor = System.Drawing.Color.DimGray;
            this.txtVehicle.PlaceholderText = "Mazda";
            this.txtVehicle.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVehicle.SelectedText = "";
            this.txtVehicle.Size = new System.Drawing.Size(307, 29);
            this.txtVehicle.TabIndex = 15;
            this.txtVehicle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Transportation_Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Name = "Transportation_Control";
            this.Size = new System.Drawing.Size(834, 540);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private Guna.UI2.WinForms.Guna2Button Confirm_btn;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2Shapes guna2Shapes5;
        private Guna.UI2.WinForms.Guna2TextBox txtArrival;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2Shapes guna2Shapes4;
        private Guna.UI2.WinForms.Guna2TextBox txtDriver;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2Shapes guna2Shapes3;
        private Guna.UI2.WinForms.Guna2TextBox txtDeparture;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Shapes guna2Shapes1;
        private Guna.UI2.WinForms.Guna2TextBox txtVehicle;
    }
}
