﻿namespace MidProjectEven
{
    partial class main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main_form));
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_close = new Guna.UI2.WinForms.Guna2Button();
            this.sidepicture = new System.Windows.Forms.PictureBox();
            this.Assessment_Components = new Guna.UI2.WinForms.Guna2Button();
            this.Assesment_btn = new Guna.UI2.WinForms.Guna2Button();
            this.Evalution_btn = new Guna.UI2.WinForms.Guna2Button();
            this.Clo_btn = new Guna.UI2.WinForms.Guna2Button();
            this.attendance_btn = new Guna.UI2.WinForms.Guna2Button();
            this.Students_btn = new Guna.UI2.WinForms.Guna2Button();
            this.btn_home = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.minimize_btn = new Guna.UI2.WinForms.Guna2Button();
            this.maximize_btn = new Guna.UI2.WinForms.Guna2Button();
            this.label_header_main = new System.Windows.Forms.Label();
            this.close_btn = new Guna.UI2.WinForms.Guna2Button();
            this.main_panel = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sidepicture)).BeginInit();
            this.guna2Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.BorderRadius = 35;
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6D;
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Controls.Add(this.sidepicture);
            this.panel2.Controls.Add(this.Assessment_Components);
            this.panel2.Controls.Add(this.Assesment_btn);
            this.panel2.Controls.Add(this.Evalution_btn);
            this.panel2.Controls.Add(this.Clo_btn);
            this.panel2.Controls.Add(this.attendance_btn);
            this.panel2.Controls.Add(this.Students_btn);
            this.panel2.Controls.Add(this.btn_home);
            this.panel2.Controls.Add(this.guna2Panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(180, 648);
            this.panel2.TabIndex = 1;
            // 
            // btn_close
            // 
            this.btn_close.BorderColor = System.Drawing.Color.White;
            this.btn_close.CheckedState.FillColor = System.Drawing.Color.Red;
            this.btn_close.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_close.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_close.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_close.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_close.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.btn_close.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btn_close.ForeColor = System.Drawing.Color.White;
            this.btn_close.HoverState.FillColor = System.Drawing.Color.Red;
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_close.Location = new System.Drawing.Point(83, 593);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(45, 38);
            this.btn_close.TabIndex = 1;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // sidepicture
            // 
            this.sidepicture.Image = ((System.Drawing.Image)(resources.GetObject("sidepicture.Image")));
            this.sidepicture.Location = new System.Drawing.Point(141, 91);
            this.sidepicture.Name = "sidepicture";
            this.sidepicture.Size = new System.Drawing.Size(39, 101);
            this.sidepicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.sidepicture.TabIndex = 4;
            this.sidepicture.TabStop = false;
            // 
            // Assessment_Components
            // 
            this.Assessment_Components.BackColor = System.Drawing.Color.Transparent;
            this.Assessment_Components.BorderColor = System.Drawing.Color.White;
            this.Assessment_Components.BorderRadius = 25;
            this.Assessment_Components.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.Assessment_Components.CheckedState.FillColor = System.Drawing.Color.White;
            this.Assessment_Components.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Assessment_Components.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.Assessment_Components.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Assessment_Components.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Assessment_Components.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Assessment_Components.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Assessment_Components.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Assessment_Components.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Assessment_Components.ForeColor = System.Drawing.Color.White;
            this.Assessment_Components.Image = ((System.Drawing.Image)(resources.GetObject("Assessment_Components.Image")));
            this.Assessment_Components.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Assessment_Components.Location = new System.Drawing.Point(31, 441);
            this.Assessment_Components.Name = "Assessment_Components";
            this.Assessment_Components.Size = new System.Drawing.Size(149, 43);
            this.Assessment_Components.TabIndex = 8;
            this.Assessment_Components.Text = "Assessment Component";
            this.Assessment_Components.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Assessment_Components.UseTransparentBackground = true;
            this.Assessment_Components.CheckedChanged += new System.EventHandler(this.btn_home_CheckedChanged);
            this.Assessment_Components.Click += new System.EventHandler(this.Assessment_Components_Click);
            // 
            // Assesment_btn
            // 
            this.Assesment_btn.BackColor = System.Drawing.Color.Transparent;
            this.Assesment_btn.BorderColor = System.Drawing.Color.White;
            this.Assesment_btn.BorderRadius = 25;
            this.Assesment_btn.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.Assesment_btn.CheckedState.FillColor = System.Drawing.Color.White;
            this.Assesment_btn.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Assesment_btn.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            this.Assesment_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Assesment_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Assesment_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Assesment_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Assesment_btn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Assesment_btn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Assesment_btn.ForeColor = System.Drawing.Color.White;
            this.Assesment_btn.Image = ((System.Drawing.Image)(resources.GetObject("Assesment_btn.Image")));
            this.Assesment_btn.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Assesment_btn.Location = new System.Drawing.Point(31, 381);
            this.Assesment_btn.Name = "Assesment_btn";
            this.Assesment_btn.Size = new System.Drawing.Size(149, 43);
            this.Assesment_btn.TabIndex = 7;
            this.Assesment_btn.Text = "Assessment";
            this.Assesment_btn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Assesment_btn.UseTransparentBackground = true;
            this.Assesment_btn.CheckedChanged += new System.EventHandler(this.btn_home_CheckedChanged);
            this.Assesment_btn.Click += new System.EventHandler(this.Assesment_btn_Click);
            // 
            // Evalution_btn
            // 
            this.Evalution_btn.BackColor = System.Drawing.Color.Transparent;
            this.Evalution_btn.BorderColor = System.Drawing.Color.White;
            this.Evalution_btn.BorderRadius = 25;
            this.Evalution_btn.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.Evalution_btn.CheckedState.FillColor = System.Drawing.Color.White;
            this.Evalution_btn.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Evalution_btn.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            this.Evalution_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Evalution_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Evalution_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Evalution_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Evalution_btn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Evalution_btn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Evalution_btn.ForeColor = System.Drawing.Color.White;
            this.Evalution_btn.Image = ((System.Drawing.Image)(resources.GetObject("Evalution_btn.Image")));
            this.Evalution_btn.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Evalution_btn.Location = new System.Drawing.Point(31, 501);
            this.Evalution_btn.Name = "Evalution_btn";
            this.Evalution_btn.Size = new System.Drawing.Size(171, 43);
            this.Evalution_btn.TabIndex = 6;
            this.Evalution_btn.Text = "Evaluation";
            this.Evalution_btn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Evalution_btn.UseTransparentBackground = true;
            this.Evalution_btn.CheckedChanged += new System.EventHandler(this.btn_home_CheckedChanged);
            this.Evalution_btn.Click += new System.EventHandler(this.Evalution_btn_Click);
            // 
            // Clo_btn
            // 
            this.Clo_btn.BackColor = System.Drawing.Color.Transparent;
            this.Clo_btn.BorderColor = System.Drawing.Color.White;
            this.Clo_btn.BorderRadius = 25;
            this.Clo_btn.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.Clo_btn.CheckedState.FillColor = System.Drawing.Color.White;
            this.Clo_btn.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Clo_btn.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            this.Clo_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Clo_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Clo_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Clo_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Clo_btn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Clo_btn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Clo_btn.ForeColor = System.Drawing.Color.White;
            this.Clo_btn.Image = ((System.Drawing.Image)(resources.GetObject("Clo_btn.Image")));
            this.Clo_btn.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Clo_btn.Location = new System.Drawing.Point(30, 322);
            this.Clo_btn.Name = "Clo_btn";
            this.Clo_btn.Size = new System.Drawing.Size(149, 43);
            this.Clo_btn.TabIndex = 5;
            this.Clo_btn.Text = "Clo Rubric";
            this.Clo_btn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Clo_btn.TextOffset = new System.Drawing.Point(10, 0);
            this.Clo_btn.UseTransparentBackground = true;
            this.Clo_btn.CheckedChanged += new System.EventHandler(this.btn_home_CheckedChanged);
            this.Clo_btn.Click += new System.EventHandler(this.Clo_btn_Click);
            // 
            // attendance_btn
            // 
            this.attendance_btn.BackColor = System.Drawing.Color.Transparent;
            this.attendance_btn.BorderColor = System.Drawing.Color.White;
            this.attendance_btn.BorderRadius = 25;
            this.attendance_btn.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.attendance_btn.CheckedState.FillColor = System.Drawing.Color.White;
            this.attendance_btn.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.attendance_btn.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            this.attendance_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.attendance_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.attendance_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.attendance_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.attendance_btn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.attendance_btn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.attendance_btn.ForeColor = System.Drawing.Color.White;
            this.attendance_btn.Image = ((System.Drawing.Image)(resources.GetObject("attendance_btn.Image")));
            this.attendance_btn.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.attendance_btn.Location = new System.Drawing.Point(30, 259);
            this.attendance_btn.Name = "attendance_btn";
            this.attendance_btn.Size = new System.Drawing.Size(149, 43);
            this.attendance_btn.TabIndex = 4;
            this.attendance_btn.Text = "Attendance";
            this.attendance_btn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.attendance_btn.UseTransparentBackground = true;
            this.attendance_btn.CheckedChanged += new System.EventHandler(this.btn_home_CheckedChanged);
            this.attendance_btn.Click += new System.EventHandler(this.attendance_btn_Click);
            // 
            // Students_btn
            // 
            this.Students_btn.BackColor = System.Drawing.Color.Transparent;
            this.Students_btn.BorderColor = System.Drawing.Color.White;
            this.Students_btn.BorderRadius = 25;
            this.Students_btn.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.Students_btn.CheckedState.FillColor = System.Drawing.Color.White;
            this.Students_btn.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Students_btn.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            this.Students_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Students_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Students_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Students_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Students_btn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.Students_btn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Students_btn.ForeColor = System.Drawing.Color.White;
            this.Students_btn.Image = ((System.Drawing.Image)(resources.GetObject("Students_btn.Image")));
            this.Students_btn.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Students_btn.Location = new System.Drawing.Point(30, 196);
            this.Students_btn.Name = "Students_btn";
            this.Students_btn.Size = new System.Drawing.Size(149, 43);
            this.Students_btn.TabIndex = 3;
            this.Students_btn.Text = "Students";
            this.Students_btn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Students_btn.UseTransparentBackground = true;
            this.Students_btn.CheckedChanged += new System.EventHandler(this.btn_home_CheckedChanged);
            this.Students_btn.Click += new System.EventHandler(this.Students_btn_Click);
            // 
            // btn_home
            // 
            this.btn_home.BackColor = System.Drawing.Color.Transparent;
            this.btn_home.BorderColor = System.Drawing.Color.White;
            this.btn_home.BorderRadius = 22;
            this.btn_home.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.btn_home.Checked = true;
            this.btn_home.CheckedState.FillColor = System.Drawing.Color.White;
            this.btn_home.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.btn_home.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            this.btn_home.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_home.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_home.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_home.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_home.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.btn_home.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.btn_home.ForeColor = System.Drawing.Color.White;
            this.btn_home.Image = ((System.Drawing.Image)(resources.GetObject("btn_home.Image")));
            this.btn_home.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_home.Location = new System.Drawing.Point(30, 121);
            this.btn_home.Name = "btn_home";
            this.btn_home.Size = new System.Drawing.Size(149, 43);
            this.btn_home.TabIndex = 2;
            this.btn_home.Text = "Home";
            this.btn_home.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_home.UseTransparentBackground = true;
            this.btn_home.CheckedChanged += new System.EventHandler(this.btn_home_CheckedChanged);
            this.btn_home.Click += new System.EventHandler(this.btn_home_Click);
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.Controls.Add(this.pictureBox1);
            this.guna2Panel1.Controls.Add(this.guna2HtmlLabel1);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(180, 77);
            this.guna2Panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel1.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(39, 16);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(153, 25);
            this.guna2HtmlLabel1.TabIndex = 2;
            this.guna2HtmlLabel1.Text = "CLO MANAGER";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.minimize_btn);
            this.panel4.Controls.Add(this.maximize_btn);
            this.panel4.Controls.Add(this.label_header_main);
            this.panel4.Controls.Add(this.close_btn);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(180, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(834, 46);
            this.panel4.TabIndex = 3;
            // 
            // minimize_btn
            // 
            this.minimize_btn.BackColor = System.Drawing.Color.Transparent;
            this.minimize_btn.BorderColor = System.Drawing.Color.Transparent;
            this.minimize_btn.CheckedState.FillColor = System.Drawing.Color.Red;
            this.minimize_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.minimize_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.minimize_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.minimize_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.minimize_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.minimize_btn.FillColor = System.Drawing.Color.Transparent;
            this.minimize_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.minimize_btn.ForeColor = System.Drawing.Color.White;
            this.minimize_btn.HoverState.FillColor = System.Drawing.Color.Red;
            this.minimize_btn.Image = ((System.Drawing.Image)(resources.GetObject("minimize_btn.Image")));
            this.minimize_btn.ImageSize = new System.Drawing.Size(12, 12);
            this.minimize_btn.Location = new System.Drawing.Point(669, 0);
            this.minimize_btn.Name = "minimize_btn";
            this.minimize_btn.Size = new System.Drawing.Size(55, 46);
            this.minimize_btn.TabIndex = 4;
            this.minimize_btn.UseTransparentBackground = true;
            this.minimize_btn.Click += new System.EventHandler(this.minimize_btn_Click);
            // 
            // maximize_btn
            // 
            this.maximize_btn.BackColor = System.Drawing.Color.Transparent;
            this.maximize_btn.BorderColor = System.Drawing.Color.Transparent;
            this.maximize_btn.CheckedState.FillColor = System.Drawing.Color.Red;
            this.maximize_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.maximize_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.maximize_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.maximize_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.maximize_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.maximize_btn.FillColor = System.Drawing.Color.Transparent;
            this.maximize_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.maximize_btn.ForeColor = System.Drawing.Color.White;
            this.maximize_btn.HoverState.FillColor = System.Drawing.Color.Red;
            this.maximize_btn.Image = ((System.Drawing.Image)(resources.GetObject("maximize_btn.Image")));
            this.maximize_btn.ImageSize = new System.Drawing.Size(15, 15);
            this.maximize_btn.Location = new System.Drawing.Point(724, 0);
            this.maximize_btn.Name = "maximize_btn";
            this.maximize_btn.Size = new System.Drawing.Size(55, 46);
            this.maximize_btn.TabIndex = 3;
            this.maximize_btn.UseTransparentBackground = true;
            this.maximize_btn.Click += new System.EventHandler(this.maximize_btn_Click);
            // 
            // label_header_main
            // 
            this.label_header_main.AutoSize = true;
            this.label_header_main.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_header_main.ForeColor = System.Drawing.Color.Blue;
            this.label_header_main.Location = new System.Drawing.Point(15, 7);
            this.label_header_main.Name = "label_header_main";
            this.label_header_main.Size = new System.Drawing.Size(89, 30);
            this.label_header_main.TabIndex = 3;
            this.label_header_main.Text = "Home";
            // 
            // close_btn
            // 
            this.close_btn.BackColor = System.Drawing.Color.Transparent;
            this.close_btn.BorderColor = System.Drawing.Color.Transparent;
            this.close_btn.CheckedState.FillColor = System.Drawing.Color.Red;
            this.close_btn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.close_btn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.close_btn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.close_btn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.close_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.close_btn.FillColor = System.Drawing.Color.Transparent;
            this.close_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.close_btn.ForeColor = System.Drawing.Color.White;
            this.close_btn.HoverState.FillColor = System.Drawing.Color.Red;
            this.close_btn.Image = ((System.Drawing.Image)(resources.GetObject("close_btn.Image")));
            this.close_btn.Location = new System.Drawing.Point(779, 0);
            this.close_btn.Name = "close_btn";
            this.close_btn.Size = new System.Drawing.Size(55, 46);
            this.close_btn.TabIndex = 2;
            this.close_btn.UseTransparentBackground = true;
            this.close_btn.Click += new System.EventHandler(this.close_btn_Click);
            // 
            // main_panel
            // 
            this.main_panel.BackColor = System.Drawing.Color.White;
            this.main_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_panel.ForeColor = System.Drawing.Color.Black;
            this.main_panel.Location = new System.Drawing.Point(180, 46);
            this.main_panel.Margin = new System.Windows.Forms.Padding(0, 6, 6, 6);
            this.main_panel.Name = "main_panel";
            this.main_panel.Size = new System.Drawing.Size(834, 602);
            this.main_panel.TabIndex = 4;
            // 
            // main_form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1014, 648);
            this.Controls.Add(this.main_panel);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "main_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.main_form_Load);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sidepicture)).EndInit();
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
        private System.Windows.Forms.Panel panel2;
        private Guna.UI2.WinForms.Guna2Button close_btn;
        private Guna.UI2.WinForms.Guna2Button btn_home;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2Button attendance_btn;
        private Guna.UI2.WinForms.Guna2Button Students_btn;
        private Guna.UI2.WinForms.Guna2Button Evalution_btn;
        private Guna.UI2.WinForms.Guna2Button Clo_btn;
        private Guna.UI2.WinForms.Guna2Button Assesment_btn;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private Guna.UI2.WinForms.Guna2Button Assessment_Components;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2Button btn_close;
        private System.Windows.Forms.PictureBox sidepicture;
        private System.Windows.Forms.Label label_header_main;
        private Guna.UI2.WinForms.Guna2Button maximize_btn;
        private Guna.UI2.WinForms.Guna2Button minimize_btn;
        public System.Windows.Forms.Panel main_panel;
    }
}

